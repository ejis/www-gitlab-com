---
layout: handbook-page-toc
title: "Meltano Handbook"
---

[Meltano](https://meltano.com) is an open source platform for ELT pipelines, developed by the Meltano community and the Meltano team at GitLab.

Check out the Meltano [website](https://meltano.com/), [documentation](https://meltano.com/docs/), and [team handbook](https://meltano.com/handbook/) for more information.
