---
layout: handbook-page-toc
title: "Customer Health Scores"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

# Customer Health Scores

In order to manage customers at scale we must have a consistent way of keeping track of customer details including the current “health” of the customer. The health of a customer is affected by many different factors including product usage, support interactions, internal budgets, and TAM communication.  

The current health of a customer is also directly related to the customer’s journey with GitLab. Because of this, customer health and the running communication notes between a TAM and the customer should be closely linked.

See [Health Scoring Criteria](/handbook/customer-success/tam/triage/#health-scoring-criteria).

# Timeline Notes

Meeting/call notes are the running notes for the TAM’s regular interaction with the client. The call notes should at least cover the following topics:

 * Updates on action items from the TAM
 * New updates from the customer
   * Architectural changes
   * New deployments
   * Product feedback
 * A review of any open support tickets in Zendesk
 * Q&A
 * Current customer health from a business and operational standpoint

At the end of each customer call any changes to customer health should be reflected in the customer's Timeline (Gainsight). If the Customer Health Score drops below Yellow an issue must be created in the Account Triage Project, see details here.

Call notes should be saved in Google Drive (here) following this format:

```/Sales/Customers & Prospects/A/Acme/Acme - Meeting Notes```


See an example meeting notes [here](https://docs.google.com/document/d/1dAcHBqoRTY6qqSw27VQstCCnk5Fxc2oIsbpKs014h3g).

The rationale for saving call notes in this manner is as follows:

 * Call notes frequently contain sensitive information and are for the internal sales team and management to review.
 * Call notes are tightly linked to the Health Score and should be available in the same “pane of glass” as the Health Score.
 * A folder structure allows non-Customer Success executives and support staff to easily locate the notes in the case of an escalation.
 * The naming convention “&lt;customer&gt; - Meeting Notes” allows for fast searching using Google Cloud Search for Work (https://cloudsearch.google.com/)

# Determining TAM Sentiment and Product Risk

While TAM Sentiment and Product Risk are highly related and complementary and share much of the guidelines below, the definitions for each are as follows:

* **TAM Sentiment**: Manual measure that the TAM updates to indicate their perceived sentiment of the customer
* **Product Risk**: Manual measure updated by the TAM indicating risk of downsell or churn due to product expectations

Some common things to look out for during regular meetings are:

 * **Utilization of the product.** Is it just being used for a single facet of the GitLab experience?
 * **High number of “important” feature requests.** Is the customer trying to make GitLab work just like another legacy piece of software in their environment? A review of the GitLab workflow and best practices may be in order.
 * **High number of support requests.** Is the customer struggling to keep GitLab operational or performant? An architecture review should be considered.
 * **Missing cadence calls.** Missing cadence calls could be a sign that GitLab is being deprioritized, work with your SAL to schedule a [EBR](/handbook/customer-success/tam/ebr) or higher level discussion to evaluate the customer’s health.
 * **Failure to upgrade.** Is the customer more than a major release behind? Work with the customer to explain the new features and security updates of the current version of GitLab and address any internal technical limitations that have prevented upgrades. Develop an upgrade plan with the customer if appropriate.

The items below serve as _guidelines_ for the TAM to score the customer. The items below are not intended to be a strict or rigid set of criteria.

 #### **Red**

* They’ve told us they are downgrading/cancelling.
* No communication.
* Limited or no access to executive sponsors.
* Low user adoption.
* Loss of our GitLab champion.
* Original GitLab use-cases not achieved (Success Plan).
* Customer was acquired and parent company uses competitor.
* Complaints about the company (responsiveness, feature turnaround, missed priorities in direction).

#### **Yellow**

* Lack of response to us and/ or we haven’t done much discovery.
* Don’t have the right contacts for each key person.
* Only one primary stakeholder.
* At least one major release behind.
* Doesn't raise any tickets or contribute to any issues.
* Occasionally doesn't show up or are late to scheduled cadence calls.
* Customer doesn't know what success looks like or it is ill-defined.
* Raises more than 10 tickets a month.

#### **Green**

* Successful GitLab use-cases (Success Plans).
* Uses the full GitLab DevOps lifecycle.
* Regular communication and positive relationship.
* Healthy user adoption rate.
* Regularly show interest and utilisation in new features.
* Always show up to regular cadence calls and communicates when they can't.
* They are interested in upgrading.
* Up to date on releases.
* Raises between 1 and 5 tickets a month and regularly contributes to issues.
* Regularly provides feedback on how to improve GitLab.


# Customer Cadence

### Enterprise

Customer cadence calls should be scheduled weekly during the customer onboarding phase and at least monthly otherwise. TAM Sentiment and Product Risk fields should be updated at the end of each cadence call as part of the Timeline entry. 

### Mid-Market

Customer cadence calls should be scheduled frequently during the customer onboarding phase and at least quarterly otherwise. TAM Sentiment and Product Risk fields should be updated at the end of each cadence call as part of the Timeline entry.



