---
layout: markdown_page
title: "Customer Deployment Types"
---
## Customer Deployment Types

During customer onboarding, these fields should be filled out in Gainsight. Deployment types definitions for customer attributes on the Gainsight C360:

### Customer type

* Community conversion (first value waived) - A customer who was using the free community version and is converting to a paid subscription tier.
* New Customer - This is a new customer who doesn’t have an existing GitLab deployment.
* Existing - New TAM (first value waived) - The customer was already a paying customer and was assigned a TAM.

### Hosting

What is the customer's main deployment type?

* Self-managed - on-premises
* Self-managed - cloud
* Self-managed - hybrid
* GitLab.com
* Self-managed and GitLab.com

### Deployer

Addresses the question of *who* did the work to deploy them?

* Customer-deployed
* GitLab services
* Partner services
