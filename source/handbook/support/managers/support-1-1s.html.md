---
layout: handbook-page-toc
title: Support 1:1s
---

At GitLab we have [weekly 1-1s](https://about.gitlab.com/handbook/leadership/1-1/).

The aim of this page is to:

1. Encourage consistency across the global Support team.
1. Help make your work visible.

## Activity Links

This section will be updated when [this issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/2136) is closed. See the issue to start adding activity links at the top of your 1-1 notes doc. There are examples of links on the [Support Engineer Responsibilities](/handbook/support/support-engineer-responsibilities.html) page.
